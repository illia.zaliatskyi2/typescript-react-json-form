import React, { FC }       from 'react'
import IQuestion           from '../../types/IQuestion'
import { renderFormField } from '../../helpers/renderFormField'

const Question:FC<IQuestion > = ({
    label,
    isRequired,
    inputPlaceholder,
    displayType,
    id,
    note,
    choices
 }): JSX.Element => {
    const currentField: JSX.Element = renderFormField(displayType, {label, inputPlaceholder, isRequired, id, note, choices })

    return <div key={id} className='step-page__row-element'>
        {currentField}
    </div>
}

export default Question
