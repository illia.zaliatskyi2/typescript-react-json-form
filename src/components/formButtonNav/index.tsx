import React from 'react'
import { IFormButtonTypes } from '../../types/IFormButtonTypes'

const FormButtonNav = ({disabled, styleName, event, content}: IFormButtonTypes): JSX.Element => {

    return (
        <button// @ts-ignore
            onClick={event}
            className={styleName}
            disabled={disabled}
            type='submit'
        >{content}</button>
    )
}

export default FormButtonNav
