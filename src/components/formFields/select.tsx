import { IFormField } from '../../types/formTypes'
import IChoice from '../../types/IChoice'

const Select = ({
    inputPlaceholder,
    isRequired,
    id,
    label,
    choices,
    storageFormValue
}: IFormField): JSX.Element => {
    return (
        <>
            {label && <label className='step-page__row-label' htmlFor={`${id}`}>{label}</label>}
            <select
                className='step-page__row-select'
                placeholder={inputPlaceholder}
                required={isRequired}
                id={`${id}`}
                defaultValue={storageFormValue}
            >
                {choices.map(({label}: IChoice) => {
                    return <option
                        key={label}
                    >{label}</option>
                })}
            </select>
        </>
    )
}

export default Select
