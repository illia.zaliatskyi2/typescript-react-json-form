import { IFormField } from '../../types/formTypes'
import IChoice from '../../types/IChoice'

const Checkboxes = ({
    choices,
    label,
    id,
    storageFormValue
}: IFormField): JSX.Element => {

    return(
        <>
            {label && <label className='step-page__row-label'>{label}</label>}
            <ul className='step-page__row-checkboxes' id={`${id}`}>
                {choices.map(({label: checkboxLabel}: IChoice) => {
                    const checkboxStatus = storageFormValue ? storageFormValue[checkboxLabel] : false

                    return (
                        <li
                            key={checkboxLabel}
                            className='step-page__row-checkboxes-row'
                        >
                            <label>
                                <input
                                    type="checkbox"
                                    id={checkboxLabel}
                                    className='step-page__row-checkbox'
                                    defaultChecked={checkboxStatus}
                                />
                                {checkboxLabel}
                            </label>
                        </li>
                    )
                })}
            </ul>
        </>
    )
}

export default Checkboxes
