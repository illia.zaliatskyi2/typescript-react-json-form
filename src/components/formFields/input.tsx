import { IFormField } from '../../types/formTypes'

const FormInput = ({
   inputPlaceholder,
   id,
   label,
   storageFormValue
}: IFormField): JSX.Element => {
    return (
        <>
            {label && <label className='step-page__row-label' htmlFor={`${id}`}>{label}</label>}
            <input
                placeholder={inputPlaceholder}
                id={`${id}`}
                className='step-page__row-input'
                defaultValue={storageFormValue ? storageFormValue : ''}
            />
        </>
    )
}

export default FormInput
