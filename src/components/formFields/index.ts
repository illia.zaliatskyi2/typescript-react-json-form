import FormInput from './input'
import CustomRange from './custom-range'
import FormTextarea from './textarea'
import Select from './select'
import LoadFile from './loadFile'
import Smiles from './smiles'
import Checkboxes from './checkboxes'

export {
    FormInput,
    CustomRange,
    FormTextarea,
    Select,
    LoadFile,
    Smiles,
    Checkboxes
}
