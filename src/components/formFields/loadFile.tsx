import { IFormField } from '../../types/formTypes'

const LoadFile = ({inputPlaceholder, isRequired, id, label}: IFormField): JSX.Element => {
    return (
        <>
            {label && <label className='step-page__row-label' htmlFor={`${id}`}>{label}</label>}
            <input
                placeholder={inputPlaceholder}
                required={isRequired}
                id={`${id}`}
                type="file"
                className='step-page__row-input'
            />
        </>
)}

export default LoadFile
