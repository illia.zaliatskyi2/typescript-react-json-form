import { IFormField }      from '../../types/formTypes'
import { useState } from 'react'
import IChoice             from '../../types/IChoice'
import { useAppDispatch } from '../../redux/hooks'
import { addFormStep }    from '../../redux/reducers/formReducer'

const Smiles = ({
    choices,
    label,
    id,
    storageFormValue
}: IFormField): JSX.Element => {
    const dispatch = useAppDispatch()
    const saveSmile = (label: string) => (): void => {
        changeActiveSmile(label)
        dispatch(addFormStep({
            ...{[id]: label}
        }))
    }
    const [activeSmile, changeActiveSmile] = useState<string>(storageFormValue)

    return (
        <>
            {label && <label className='step-page__row-label'>{label}</label>}
            <div className='step-page__row-smiles'>
                {choices.map(({label, icon}: IChoice) => {
                    return (
                        <div
                            key={label}
                            className={`step-page__row-smile${activeSmile === label ? ' active' : ''}`}
                            onClick={saveSmile(label)}
                        >
                            {label}
                            <i className={`la ${icon} step-page__row-icon`}></i>
                        </div>
                    )
                })}
            </div>
        </>
    )
}

export default Smiles
