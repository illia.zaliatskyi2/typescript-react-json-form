import { IFormField } from '../../types/formTypes'
import React          from 'react'

const CustomRange = ({
    note,
    id,
    label,
    storageFormValue
 }: IFormField): JSX.Element => {
    return (
        <>
            {note && <p>note: {note}. Step is 2</p>}
            {label && <label className='step-page__row-label' htmlFor={`${id}`}>{label}</label>}
            <input
                type="range"
                id={`${id}`}
                name={`${id}`}
                min="0"
                max="10"
                step="2"
                defaultValue={storageFormValue ? storageFormValue : "10"}
            />
        </>
    )
}

export default CustomRange
