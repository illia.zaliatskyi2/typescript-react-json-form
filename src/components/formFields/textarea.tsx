import { IFormField } from '../../types/formTypes'

const FormTextarea = ({
    inputPlaceholder,
    isRequired,
    id,
    label,
    storageFormValue
}: IFormField): JSX.Element => {
    return (
        <>
            {label && <label className='step-page__row-label' htmlFor={`${id}`}>{label}</label>}
            <textarea
                placeholder={inputPlaceholder}
                required={isRequired}
                id={`${id}`}
                className='step-page__row-input'
                defaultValue={storageFormValue}
            />
        </>
    )
}

export default FormTextarea
