import React, { FC } from 'react'
import IPage         from '../../types/IPage'
import IQuestion     from '../../types/IQuestion'
import Question      from '../question'
import './StepPage.scss'

const FormPage:FC<IPage> = ({
    questions,
    text,
    title,
    }): JSX.Element => {
    return (
        <div className='step-page__row'>
            {title && <h2>{title}</h2>}
            <div className='step-page__row-questions'>
                {questions.length > 0 ?
                    questions.map(({ref, ...questionProps}: IQuestion) => <Question {...questionProps} key={ref} /> )
                    :
                    <div>{text && <p dangerouslySetInnerHTML={{__html: text }}></p>}</div>
                }
            </div>
        </div>
    )
}

export default FormPage
