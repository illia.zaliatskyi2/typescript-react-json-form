import checkStorageExistence from '../../helpers/checkStorageExistence'

const ResultsPage = (): JSX.Element => {
    const storageValues = checkStorageExistence()
    const storageKeys = Object.keys(storageValues)

    return (
        <div>
            {
                storageKeys.map((key , index) => {
                    const value = storageValues[key]
                    const renderResult = typeof value !== 'object' ?
                        value :
                        Object.keys(storageValues[key]).map((rowKey: any) => <div>{rowKey}: {`${storageValues[key][rowKey]}`}</div>)

                    return (
                        <>
                            <div>Step {index + 1}:</div>
                            <div>Answer: {renderResult}</div>
                        </>
                    )
                })
            }
        </div>
    )
}


export default ResultsPage
