import React, { useRef, useState } from 'react'
import FormPage                           from './components/step-page'
import FormButtonNav                      from './components/formButtonNav'
import { useAppDispatch, useAppSelector } from './redux/hooks'
import { nextStep, prevStep }             from './redux/reducers/stepsReducer'
import getFormValues                      from './helpers/getFormValues'
import { addFormStep }                    from './redux/reducers/formReducer'
import myData                             from './dump_swift.json'
import IJson                              from './types/IJson'
import IPage                              from './types/IPage'
import { AppProps }                       from './types/IAppProps'
import './App.scss'
import ResultsPage                        from './components/results-page'

const App = <T extends AppProps>(props: T): JSX.Element => {
    const currentStep = useAppSelector<number>(state => state.steps.value)
    const [showFinalForm, changeFinalForm] = useState(false)
    const dispatch = useAppDispatch()
    const formRef = useRef<HTMLFormElement>(null);
    const {styling, label, pages} = myData as IJson
    const isFirstPage = currentStep === 1
    const isLastPage = currentStep + 1 === pages.length - 1
    const handlePrevStep = (): void => {
        formSubmitHandler()
        dispatch(prevStep({
            value: currentStep - 1
        }))
    }
    const handleNextStep = (): void => {
        formSubmitHandler()
        dispatch(nextStep({
            value: currentStep + 1
        }))
    }
    const formSubmitHandler = (): void => {
        const form = formRef.current

        if (form) {
            dispatch(addFormStep({
                ...getFormValues(form.elements)
            }))
        }
    }

    return (
        <div className="App">
            <header><img src={styling.logoPath} width={100} alt="" /></header>
            <main className="main">
                <h1>{label}</h1>
                {!showFinalForm ? <div className='step-page'>
                    <h2>Step: {currentStep}</h2>
                    <form ref={formRef}>
                        {pages.map((page: IPage, index): JSX.Element | null => {
                            const {ref, ...pageInfo} = page

                            return currentStep === index ? <FormPage {...pageInfo} key={`${ref}-${index}`} /> : null
                        })}
                    </form>
                    <div className="step-btns">
                        <FormButtonNav
                            event={handlePrevStep}
                            styleName={"step-btn step-btn__prev"}
                            disabled={isFirstPage}
                            content={'prev step'}
                        />
                        {!isLastPage ? <FormButtonNav
                          event={handleNextStep}
                          styleName={"step-btn step-btn__next"}
                          disabled={false}
                          content={'next step'}
                        /> : <FormButtonNav
                            event={() => changeFinalForm(true)}
                            styleName={"step-btn step-btn__next"}
                            disabled={false}
                            content={'submit form'}
                        />}
                    </div>
                </div>: <ResultsPage /> }
            </main>
        </div>
    )
}

export default App
