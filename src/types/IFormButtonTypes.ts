import React from 'react'

export interface IFormButtonTypes {
    disabled: boolean,
    styleName: string,
    event(e: React.ChangeEvent<HTMLInputElement>): void,
    content: string
}
