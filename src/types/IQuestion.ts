import IChoice from './IChoice'

export default interface IQuestion {
    allowText: boolean,
    choices: IChoice[],
    displayType: any,
    id: number,
    inputPlaceholder: string,
    isRequired: boolean,
    label: string,
    note: string | null,
    ref?: string,
}
