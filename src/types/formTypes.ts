import IChoice from './IChoice'

interface IFormField {
    label: string,
    inputPlaceholder: string,
    isRequired: boolean,
    id: number | string,
    note?: string | null,
    choices: IChoice[],
    storageFormValue?: any
}

export type {
    IFormField
}
