import IPage                from './IPage'
import { IObjectInterface } from './IObjectInterface'

export default interface IJson {
    displayContactMe: boolean,
    displayPrivacyOption: boolean,
    flightId: string,
    getVars: IObjectInterface,
    id: string,
    label: string,
    locale: string,
    pages: IPage[],
    redirect: string,
    rules: object[],
    skin: string,
    styling: IObjectInterface
}
