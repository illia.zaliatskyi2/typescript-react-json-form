import IQuestion from './IQuestion'

export default interface IPage {
    questions: IQuestion[],
    ref?: string,
    text: string,
    title: string,
    type: string
}
