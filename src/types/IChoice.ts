export default interface IChoice {
    icon: string | null,
    id: string,
    label: string,
    ref: string,
    score?: number
}