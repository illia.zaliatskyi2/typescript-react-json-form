interface IObjectInterface { [key: string]: any }

export type {
    IObjectInterface
}
