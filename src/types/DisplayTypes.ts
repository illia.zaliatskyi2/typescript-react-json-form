export enum DisplayTypes {
    upload= 'upload',
    list = 'select',
    likert = 'custom-range',
    textmultiline = 'textarea',
    textline = 'input',
    ranking = 'custom select',
    range = 'range',
    multiplechoice = 'checkbox',
    smiley = 'custom-smiles'
}
