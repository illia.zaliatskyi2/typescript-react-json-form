import { createSlice, current, PayloadAction } from '@reduxjs/toolkit'
const initialValues = <Object>[]

export const formSlice = createSlice({
    name: 'form',
    initialState: {
        value: initialValues
    },
    reducers: {
        addFormStep: (state, {payload}: PayloadAction<object>) => {
            const { value } = current(state)

            state.value = {...value, ...payload}
        },
    }
})
// Action creators are generated for each case reducer function
export const {
    addFormStep
} = formSlice.actions
export default formSlice.reducer
