import { createSlice, current } from '@reduxjs/toolkit'

const initialStep = 1

export const stepsSlice = createSlice({
    name: 'form',
    initialState: {
        value: initialStep
    },
    reducers: {
        prevStep: (state, action) => {
            const { value } = current(state)

            state.value = value - 1
        },
        nextStep: (state, action) => {
            const { value } = current(state)

            state.value = value + 1
        },
    }
})
// Action creators are generated for each case reducer function
export const {
    prevStep,
    nextStep
} = stepsSlice.actions
export default stepsSlice.reducer
