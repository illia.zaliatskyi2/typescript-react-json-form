import { combineReducers } from 'redux'
import FormReducer from './formReducer'
import StepsReducer from './stepsReducer'

const reducers = combineReducers({
    form: FormReducer,
    steps: StepsReducer
})

export default reducers
