import { applyMiddleware, createStore } from 'redux'
import reducers from './reducers'
import localStorageMiddleware from './middleware'

const middlewareEnhancer = applyMiddleware(localStorageMiddleware)

const store = createStore(reducers, middlewareEnhancer)

export default store


// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {form: formState}
export type AppDispatch = typeof store.dispatch
