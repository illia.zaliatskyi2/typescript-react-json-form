import { Dispatch, Middleware, MiddlewareAPI } from 'redux'

const localStorageMiddleware: Middleware = (store: MiddlewareAPI) => (next: Dispatch) => (action: any) => {
    const { form } = store.getState()
    const res = next(action)

    localStorage.setItem('form', JSON.stringify(form.value))

    return res
}

export default localStorageMiddleware
