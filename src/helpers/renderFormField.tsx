import { DisplayTypes } from '../types/DisplayTypes'
import { IFormField } from '../types/formTypes'
import {
    Checkboxes,
    CustomRange,
    FormInput,
    FormTextarea,
    LoadFile,
    Select,
    Smiles
}  from '../components/formFields'
import checkStorageExistence from './checkStorageExistence'

type field = keyof typeof DisplayTypes

export const renderFormField = (type: field, props: IFormField) => {
    const storageValues = checkStorageExistence()
    let storageFormValue

    Object.keys(storageValues).forEach(key => {
        if (+key === props.id) storageFormValue = storageValues[key]
    })
    const componentProps = {...props, storageFormValue}

    switch(type) {
          case 'textline':
              return <FormInput {...componentProps} />
          case 'likert':
          case 'range':
              return <CustomRange {...componentProps} />
          case 'textmultiline':
              return <FormTextarea {...componentProps} />
          case 'list':
              return <Select {...componentProps} />
          case 'upload':
              return <LoadFile {...componentProps} />
          case 'smiley':
              return <Smiles {...componentProps} />
          case 'ranking':
              return <Select {...componentProps} />
          case 'multiplechoice':
              return <Checkboxes {...componentProps} />
          default:
              return (
                  <div>there is no element</div>
              )
    }
}
