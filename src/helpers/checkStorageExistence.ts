import { IObjectInterface } from '../types/IObjectInterface'

const checkStorageExistence = (): IObjectInterface => {
    return JSON.parse(localStorage.getItem('form') || '{}')
}

export default checkStorageExistence
