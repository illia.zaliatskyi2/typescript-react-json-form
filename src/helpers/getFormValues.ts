import { IObjectInterface } from '../types/IObjectInterface'

const getFormValues = (formFields: any): IObjectInterface => {
    let newState: IObjectInterface = {}

    for (let element of formFields) {
        const tagName = element.tagName.toLowerCase()

        //condition for simple form elements like range, input, textarea and select
        if (tagName !== 'button' && element.type !== 'checkbox') {
            newState[element.id] = element.value
        }

        //condition for checkboxes logic
        if (element.type === 'checkbox') {
            const formFieldId = element.closest('ul').id
            const optionChecked = {[element.id]: element.checked}

            newState[formFieldId] = {...newState[formFieldId], ...optionChecked}
        }
    }

    console.error('new state is', newState)

    return newState
}


export default getFormValues
